def merge_sort(array):
    """
    Сортировка слиянием. Сложность O(n log(n))
    :param array: Список, который необходимо отсортировать
    :return: Отсортированный список
    """
    tmp = array.copy()
    n = len(tmp)
    if n == 1:
        return tmp
    left = merge_sort(tmp[:n // 2])
    right = merge_sort(tmp[n // 2:])

    size_l = len(left)
    size_r = len(right)
    i, j = 0, 0
    result = []

    while i <= size_l and j <= size_r:
        if left[i] <= right[j]:
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1

    if i != size_l:
        result += left[i:]
    else:
        result += right[j:]
    return result


if __name__ == "__main__":
    arr = [1, 5, 2, 66, 21, 2, 7]
    print(arr)
    print(merge_sort(arr))
    print(arr)
