import operator

OPERATORS = {
    "+": (1, operator.add),
    "-": (1, operator.sub),
    "*": (2, operator.mul),
    "/": (2, operator.truediv),
}


def parse(formula):
    parsed = []
    number = ""
    for s in formula:
        if s in "1234567890.":
            number += s
        elif number:
            parsed.append(float(number))
            number = ""

        if s in OPERATORS or s in "()":
            parsed.append(s)
    if number:
        parsed.append(float(number))
    return parsed


def shunting_yard(parsed):
    stack = []
    queue = []

    for token in parsed:
        if token in OPERATORS:
            while stack and stack[-1] != "(" and OPERATORS[token][0] <= OPERATORS[stack[-1]][0]:
                queue.append(stack.pop())
            stack.append(token)

        elif token == ")":
            while stack:
                x = stack.pop()
                if x == "(":
                    break
                else:
                    queue.append(x)
        elif token == "(":
            stack.append("(")
        else:
            queue.append(token)
    while stack:
        queue.append(stack.pop())
    return queue


def calc(notation):
    stack = []
    for token in notation:
        if token in OPERATORS:
            op2, op1 = stack.pop(), stack.pop()
            stack.append(OPERATORS[token][1](op1, op2))
        else:
            stack.append(token)
    return stack[0]


if __name__ == "__main__":
    print(calc(shunting_yard(parse("(2.0 + 2) * 2"))))
