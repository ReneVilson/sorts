def bibble_sort(array):
    """
    Сортировка пузырьком. Сложность O(n^2)
    :param array: Список который необходимо отсортировать
    :return: Отсортированный список
    """
    n = len(array)
    tmp = array.copy()
    for i in range(n):
        for j in range(n - 1):
            if tmp[j] >= tmp[j + 1]:
                tmp[j], tmp[j + 1] = tmp[j + 1], tmp[j]
    return tmp


if __name__ == "__main__":
    arr = [1, 5, 2, 66, 21, 2, 7]
    print(arr)
    print(bibble_sort(arr))
    print(arr)
