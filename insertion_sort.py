def insertion_sort(array):
    """
    Сортировка вставками. Сложность O(n^2)
    :param array: Список, который необходимо отсортировать
    :return: Отсортированный список
    """
    n = len(array)
    tmp = array.copy()

    for i in range(n):
        j = i
        val = tmp[i]

        while j > 0 and tmp[j - 1] >= val:
            tmp[j] = tmp[j - 1]
            j -= 1

        tmp[j] = val
    return tmp


if __name__ == "__main__":
    arr = [1, 5, 2, 66, 21, 2, 7]
    print(arr)
    print(insertion_sort(arr))
    print(arr)
