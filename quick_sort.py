def quick_sort(array):
    """
    БЫстрая сортировка. Сложность O(n log(n))
    :param array: Список, который необходимо отсортировать
    :return: Отсортированный список
    """
    tmp = array.copy()
    n = len(tmp)
    if n <= 1:
        return tmp
    else:
        pivot = tmp[0]
        left = quick_sort([x for x in tmp[1:] if x < pivot])
        right = quick_sort([x for x in tmp[1:] if x >= pivot])
        return left + [pivot, ] + right


if __name__ == "__main__":
    arr = [1, 5, 2, 66, 21, 2, 7]
    print(arr)
    print(quick_sort(arr))
    print(arr)
